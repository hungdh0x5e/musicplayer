package com.hungdh.music.adapter;

import android.view.View;

/**
 * Created by hungdh on 25/03/2016.
 */
public interface OnItemClickListener {
    public void onItemClick(View v, int position);
}
