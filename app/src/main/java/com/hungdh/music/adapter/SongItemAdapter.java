package com.hungdh.music.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.hungdh.music.R;
import com.hungdh.music.model.Song;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hungdh on 25/03/2016.
 */
public class SongItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<Song> songList = new ArrayList<>();
    private LayoutInflater inflater;
    private final String TAG = this.getClass().getSimpleName();

    private OnItemClickListener mOnItemClickListener;

    private final int TYPE_POST = 0;
    private final int TYPE_PROG = 1;

    public SongItemAdapter(Context mContext, List<Song> songList) {
        this.mContext = mContext;
        this.songList = songList;

        this.inflater = LayoutInflater.from(mContext);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        if (viewType == TYPE_POST) {
            View view = inflater.inflate(R.layout.song_item, parent, false);
            viewHolder = new SongHolder(view, mOnItemClickListener);

        } else {
//            View view = inflater.inflate(R.layout.song_item, parent, false);
//            viewHolder = new ProgressViewHolder(view);
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SongHolder) {
            SongHolder songHolder = (SongHolder) holder;
            Song song = songList.get(position);

            songHolder.title.setText(song.getSongTitle());
            songHolder.author.setText(song.getSongAuthor());
            if (!song.getSongImage().isEmpty())
                Picasso.with(mContext).load(song.getSongImage())
                        .placeholder(R.drawable.nav_nhacche)
                        .error(R.drawable.nav_vietmix)
                        .into(songHolder.image);
            else
                songHolder.image.setImageResource(R.drawable.nav_vietmix);
        }
    }

    @Override
    public int getItemCount() {
        return songList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return songList.get(position) == null ? TYPE_PROG : TYPE_POST;
    }

    public static class SongHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView title, author;
        private ImageView image;
        private OnItemClickListener mOnItemClickListener;
        private ImageButton play;

        public SongHolder(View itemView, OnItemClickListener listener) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.song_title);
            author = (TextView) itemView.findViewById(R.id.song_author);
            image = (ImageView) itemView.findViewById(R.id.song_image);
            play = (ImageButton) itemView.findViewById(R.id.btn_play);
            play.setOnClickListener(this);

            mOnItemClickListener = listener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mOnItemClickListener.onItemClick(v, getAdapterPosition());
        }
    }
}
