package com.hungdh.music.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.hungdh.music.R;
import com.hungdh.music.model.Song;
import com.hungdh.music.service.PlayerService;

import java.util.concurrent.TimeUnit;

public class PlayerActivity extends AppCompatActivity implements View.OnClickListener {

    private Animation mAnimation;
    private Song mSong;

    private ImageView rotate;
    private ImageButton btnPlayer, btnNext, btnPrev;
    private TextView txtCurrent, txtTotal;
    private SeekBar seekBar;
    private ProgressBar loadingBar;

    private boolean isPlay = true;


    // Action for intent
    public static final String SEND_SONG = "sendSong";
    public static final String SEND_DURATION = "sendDuration";
    public static final String SEND_PROGRESS = "sendProgress";
    public static final String SEND_LOAD_COMPLETE = "sendLoadComplete";
    public static final String SEND_PLAY_COMPLETE = "sendPlayComplete";
    public static final String SEND_STOP = "sendStop";
    // Action for Play, Next, Prev, Seek
    public static final String SEND_STATUS = "sendStatus";
    public static final String SEND_SEEK_BAR = "sendSeekBar";
    public static final String SEND_NEXT = "sendNext";
    public static final String SEND_PREV = "sendPrevious";

    // Key for intent
    public static final String DURATION = "duration";
    public static final String PROGRESS = "progress";
    public static final String CURRENT_POSITION = "currentPosition";
    public static final String SEEK_BAR_POSITION = "seekBarPosition";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getWidgetForm();
        handleIntent();

        registerIntentFilter();

        sendToService();

    }


    private void getWidgetForm() {
        rotate = (ImageView) findViewById(R.id.rotate);
        mAnimation = AnimationUtils.loadAnimation(this, R.anim.clockwise_rotation);

        loadingBar = (ProgressBar) findViewById(R.id.load_progress);
        loadingBar.setVisibility(View.VISIBLE);

        btnPlayer = (ImageButton) findViewById(R.id.btn_play);
        btnPlayer.setOnClickListener(this);
        btnNext = (ImageButton) findViewById(R.id.btn_next);
        btnNext.setOnClickListener(this);
        btnPrev = (ImageButton) findViewById(R.id.btn_prev);
        btnPrev.setOnClickListener(this);

        txtCurrent = (TextView) findViewById(R.id.current_position_label);
        txtTotal = (TextView) findViewById(R.id.total_duration_label);

        seekBar = (SeekBar) findViewById(R.id.song_progress_bar);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                loadingBar.setVisibility(View.VISIBLE);

                sendCurrentPositionToService(seekBar.getProgress());
            }
        });

    }

    private void handleIntent() {
        mSong = (Song) getIntent().getBundleExtra(SEND_SONG).getSerializable(SEND_SONG);
    }

    private void registerIntentFilter() {
        IntentFilter filter = new IntentFilter(SEND_DURATION);
        filter.addAction(SEND_PROGRESS);
        filter.addAction(SEND_LOAD_COMPLETE);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiverFromService(), filter);
    }

    private void sendToService() {
//        Intent intent = new Intent(this, PlayerService.class);
        Intent intent = new Intent(PlayerService.SERVICE_NAME);
        Bundle bundle = new Bundle();
        bundle.putSerializable(SEND_SONG, mSong);
        intent.putExtra(SEND_SONG, bundle);
        startService(intent);
    }


    public void startAnimation() {
        rotate.startAnimation(mAnimation);
    }

    public void stopAnimation() {
        rotate.clearAnimation();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_play:
                changeStatusMedia();
                break;
            case R.id.btn_next:
                sendToService();
                break;
            case R.id.btn_prev:
                stopService(new Intent(PlayerService.SERVICE_NAME));
                break;

        }
    }

    private void changeStatusMedia() {
        Intent intent = new Intent(SEND_STATUS);
        isPlay = !isPlay;
        btnPlayer.setImageResource(isPlay ? R.drawable.ic_btn_pause : R.drawable.ic_btn_play);
        if (isPlay) startAnimation();
        else stopAnimation();
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }


    private void sendCurrentPositionToService(int seekBarPosition) {
        Intent intent = new Intent(SEND_SEEK_BAR);
        intent.putExtra(SEEK_BAR_POSITION, seekBarPosition);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private BroadcastReceiver receiverFromService() {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {
                    case SEND_DURATION:
                        int duration = intent.getIntExtra(DURATION, 0);
                        txtTotal.setText(formatTime(duration));
                        break;
                    case SEND_PROGRESS:
                        int progress = intent.getIntExtra(PROGRESS, 0);
                        int current_position = intent.getIntExtra(CURRENT_POSITION, 0);
                        txtCurrent.setText(formatTime(current_position));
                        seekBar.setProgress(progress);
                        break;
                    case SEND_LOAD_COMPLETE:
                        loadingBar.setVisibility(View.GONE);
                        btnPlayer.setImageResource(R.drawable.ic_btn_pause);
                        startAnimation();
                        break;
                    case SEND_PLAY_COMPLETE:
                        changeStatusMedia();
                        stopAnimation();
                        break;
                }
            }
        };
    }

    private String formatTime(int milliseconds) {
        return String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(milliseconds),
                TimeUnit.MILLISECONDS.toSeconds(milliseconds)
                        - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliseconds)));
    }
}
