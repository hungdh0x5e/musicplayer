package com.hungdh.music.activity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RemoteViews;

import com.hungdh.music.R;
import com.hungdh.music.adapter.OnItemClickListener;
import com.hungdh.music.adapter.SongItemAdapter;
import com.hungdh.music.customview.DividerItemDecoration;
import com.hungdh.music.data.GetDataFromAPI;
import com.hungdh.music.data.ListSong;
import com.hungdh.music.model.Song;
import com.hungdh.music.service.PlayerService;

import java.io.IOException;
import java.util.List;

import static com.hungdh.music.activity.PlayerActivity.SEND_SONG;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private List<Song> songList;
    private SongItemAdapter adapter;

    private OnItemClickListener mItemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(View v, int position) {
            ListSong.setSelectedItem(position);
            Bundle bundle = new Bundle();
            bundle.putSerializable(SEND_SONG, songList.get(position));
            Intent i = new Intent(getApplicationContext(), PlayerActivity.class);
            i.putExtra(SEND_SONG, bundle);
            startActivity(i);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getWidgetForm();

        setupRecyclerView();

      //  CheckData();
    }

    private void CheckData() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String tokenResponse = GetDataFromAPI.getToken();
                    Log.d("Main", "TokenReponse\n" + tokenResponse);
//                    Log.d("Main", "ListCategory\n" + GetDataFromAPI.getListCategory(token));
//                    Log.d("Main", "ListSong\n" + GetDataFromAPI.getListSongByCategory(token, 5));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();


    }

    private void getWidgetForm() {
        mRecyclerView = (RecyclerView) findViewById(R.id.list_song);
    }


    private void setupRecyclerView() {
        songList = ListSong.getListSong();
        adapter = new SongItemAdapter(this, songList);
        adapter.setOnItemClickListener(mItemClickListener);
        mRecyclerView.setAdapter(adapter);
        LinearLayoutManager layout = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layout);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, null));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
         customSimpleNotification(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        stopService(new Intent(PlayerService.SERVICE_NAME));
        super.onBackPressed();
    }





    static String songName = "Now You're Gone";
    static String albumName = "Now Youre Gone - The Album";

    private static boolean currentVersionSupportBigNotification = false;
    private static boolean currentVersionSupportLockScreenControls = false;

    public static void customSimpleNotification(Context context) {
        RemoteViews simpleView = new RemoteViews(context.getPackageName(), R.layout.custom_notification);

        Notification notification = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_music)
                .setContentTitle("Custom Big View").build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.contentView = simpleView;
        notification.contentView.setTextViewText(R.id.textSongName, songName);
        notification.contentView.setTextViewText(R.id.textAlbumName, albumName);

        setListeners(simpleView, context);

        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(7, notification);
    }

    public static final String NOTIFY_PREVIOUS = "com.tutorialsface.notificationdemo.previous";
    public static final String NOTIFY_DELETE = "com.tutorialsface.notificationdemo.delete";
    public static final String NOTIFY_PAUSE = "com.tutorialsface.notificationdemo.pause";
    public static final String NOTIFY_PLAY = "com.tutorialsface.notificationdemo.play";
    public static final String NOTIFY_NEXT = "com.tutorialsface.notificationdemo.next";

    private static void setListeners(RemoteViews view, Context context) {
        Intent previous = new Intent(NOTIFY_PREVIOUS);
        Intent delete = new Intent(NOTIFY_DELETE);
        Intent pause = new Intent(NOTIFY_PAUSE);
        Intent next = new Intent(NOTIFY_NEXT);
        Intent play = new Intent(NOTIFY_PLAY);

//        PendingIntent pPrevious = PendingIntent.getBroadcast(context, 0, previous, PendingIntent.FLAG_UPDATE_CURRENT);
//        view.setOnClickPendingIntent(R.id.btnPrevious, pPrevious);

        PendingIntent pDelete = PendingIntent.getBroadcast(context, 0, delete, PendingIntent.FLAG_UPDATE_CURRENT);
        view.setOnClickPendingIntent(R.id.btnDelete, pDelete);

        PendingIntent pPause = PendingIntent.getBroadcast(context, 0, pause, PendingIntent.FLAG_UPDATE_CURRENT);
        view.setOnClickPendingIntent(R.id.btnPause, pPause);

        PendingIntent pNext = PendingIntent.getBroadcast(context, 0, next, PendingIntent.FLAG_UPDATE_CURRENT);
        view.setOnClickPendingIntent(R.id.btnNext, pNext);

        PendingIntent pPlay = PendingIntent.getBroadcast(context, 0, play, PendingIntent.FLAG_UPDATE_CURRENT);
        view.setOnClickPendingIntent(R.id.btnPlay, pPlay);
    }
}
