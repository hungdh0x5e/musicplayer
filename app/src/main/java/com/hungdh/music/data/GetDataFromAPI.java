package com.hungdh.music.data;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by hungdh on 01/04/2016.
 */
public class GetDataFromAPI {
    private static final String URL_NAME = "http://app.nhacdj.vn/api2/get.php?";
    private static final OkHttpClient client = new OkHttpClient();

    public static String getToken() throws IOException {
        String urlGetToken = String.format("%sact=getToken&a1=nhacdj&a2=matxac&a3=%s&a4=%s",
                URL_NAME, getDateSystemUI(), getTokenKey());
        return fetchData(urlGetToken);
    }


    public static String getListCategory(String token) throws IOException {
        String urlListCategory = String.format("%sact=listCategory&a1=%s&type=music",
                URL_NAME, token);
        return fetchData(urlListCategory);
    }

    public static String getListSongByCategory(String token, int catID) throws IOException {
        /**
         * a1=zg0pkmvxdo7xlwk4gtvam6s612flqt0q90ftnar1zyzlvkpbdk
         &act=listArticleCat
         &cat=5 // Cat ID
         &type=music
         &range=year
         &sort=date
         &limit=10
         &page=1
         */
        String endpoint = String.format("%sact=listArticleCat&cat=%d&a1=%s&type=music&range=year&sort=date&limit=10&page=1",
                URL_NAME, catID, token);
        return fetchData(endpoint);
    }

    @NonNull
    private static String fetchData(String endpoint) throws IOException {
        Request request = new Request.Builder()
                .url(endpoint)
                .build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
        return response.body().string();
    }


    private static String md5UI(String in) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.reset();
            digest.update(in.getBytes());
            byte[] a = digest.digest();
            int len = a.length;
            StringBuilder sb = new StringBuilder(len << 1);
            for (int i = 0; i < len; i += 1) {
                sb.append(Character.forDigit((a[i] & 240) >> 4, 16));
                sb.append(Character.forDigit(a[i] & 15, 16));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }
    @SuppressLint({"SimpleDateFormat"})
    private static String getDateSystemUI() {
        return new StringBuilder(String.valueOf(System.currentTimeMillis() / 1000)).toString();
    }

    private static String getTokenKey() {
        String aaaa = "99e08e2f954e99fc04330d14ff22c098nhacdjmatxac" + getDateSystemUI();
        String sig = md5UI(aaaa);
        Log.e("TAG", new StringBuilder(String.valueOf(aaaa)).append("   ").append(sig).toString());
        return sig;
    }
}
