package com.hungdh.music.data;

import com.hungdh.music.model.Song;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hungdh on 25/03/2016.
 */
public class ListSong {
    private static List<Song> listSong;
    private static int selectedItem;

    public static List<Song> getListSong(){
        if(listSong == null) {
            listSong = new ArrayList<>();
            listSong.add(new Song("Việt Mixx - Ta chia tay nhau đi", "Nhạc DJ",
                    "http://upload.nhacdj.vn/Nonstop/2016/3/4/128/NhacDJ.vn-Nontop-Vit-Mix-Vy-L-Ta-Chia-Tay-DJ-Ribo-Remix1457087753.mp3",
                    "http://upload.nhacdj.vn/Photo/2014/12/30/DJTitBanRon/hinhchunhat.jpg"));

            listSong.add(new Song("Nonstop - Phiêu Theo Điệu Nhạc 2016", "Triệu Muzik",
                    "http://upload.nhacdj.vn//Nonstop/2016/3/19/128/NhacDJ.vn-Nonstop-X-Nhc-ng-Cp-Xung-o-Thi-Ri-DJ-Phc-Tu-97-Mix1458360571.mp3",
                    "http://upload.nhacdj.vn/Photo/2016/3/4/NgocTrinh/thumb.jpg"));

            listSong.add(new Song("Như Ngày Hôm Qua", "DJ Young Nguyễn",
                    "http://upload.nhacdj.vn/Nonstop/2014/10/29/128/[NhacDJ.vn] - Nonstop-Vit-Mix-Hoa-Anh-Tc-DJ-HYT-Remix1414482835 [NhacDJ.vn].mp3",
                    "http://nhacdj.vn/images/misc/avatar-dj-new.jpg"));

            listSong.add(new Song("Dang cap Chau Au", "Nhạc DJ",
                    "http://upload.nhacdj.vn//Nonstop/2016/3/19/128/NhacDJ.vn-Nonstop-X-Nhc-ng-Cp-Xung-o-Thi-Ri-DJ-Phc-Tu-97-Mix1458360571.mp3",
                    "http://upload.nhacdj.vn/Photo/2016/3/19/DoLot2/thumb.jpg"));

            listSong.add(new Song("Nhac Trẻ không lời", "Nhạc DJ",
                    "http://upload.nhacdj.vn//Nonstop/2016/2/29/128/NhacDJ.vn-Nonstop-Vit-Mix-Tuyn-Tp-Cc-Ca-Khc-Nhc-Tr-2016-DJ-L-Lo-i-Mix1456729728.mp3",
                    "http://upload.nhacdj.vn/Photo/2016/3/22/CoDau/thumb.jpg"));
            listSong.add(new Song("Việt Mixx - Ta chia tay nhau đi", "Nhạc DJ",
                    "http://upload.nhacdj.vn/Nonstop/2016/3/4/128/NhacDJ.vn-Nontop-Vit-Mix-Vy-L-Ta-Chia-Tay-DJ-Ribo-Remix1457087753.mp3",
                    "http://upload.nhacdj.vn/Photo/2014/12/30/DJTitBanRon/hinhchunhat.jpg"));

            listSong.add(new Song("Nonstop - Phiêu Theo Điệu Nhạc 2016", "Triệu Muzik",
                    "http://upload.nhacdj.vn//Nonstop/2016/3/19/128/NhacDJ.vn-Nonstop-X-Nhc-ng-Cp-Xung-o-Thi-Ri-DJ-Phc-Tu-97-Mix1458360571.mp3",
                    "http://upload.nhacdj.vn/Photo/2016/3/4/NgocTrinh/thumb.jpg"));

            listSong.add(new Song("Nonstop - Việt Mix - Khi Bên Anh Em Thấy Điều Gì", "DJ Young Nguyễn",
                    "http://upload.nhacdj.vn/Nonstop/2015/10/10/128/NhacDJ.vn-Nonstop-Vit-Mix-Khi-Bn-Anh-Em-Thy-iu-G-DJ-Young-Mix1444471935.mp3",
                    "http://nhacdj.vn/images/misc/avatar-dj-new.jpg"));

            listSong.add(new Song("Nonstop - Vina House - Tuổi Gì Familly", "Nhạc DJ",
                    "http://upload.nhacdj.vn//Nonstop/2016/3/19/128/NhacDJ.vn-Nonstop-X-Nhc-ng-Cp-Xung-o-Thi-Ri-DJ-Phc-Tu-97-Mix1458360571.mp3",
                    "http://upload.nhacdj.vn/Photo/2016/3/19/DoLot2/thumb.jpg"));

            listSong.add(new Song("Nhac Trẻ không lời", "Nhạc DJ",
                    "http://upload.nhacdj.vn/Nonstop/2014/10/27/128/[NhacDJ.vn] - Nonstop-Vina-House-Tui-G-Familly-DJ-5STYLE-Remix1414399903 [NhacDJ.vn].mp3",
                    "http://upload.nhacdj.vn/Photo/2016/3/22/CoDau/thumb.jpg"));
            listSong.add(new Song("Việt Mixx - Ta chia tay nhau đi", "Nhạc DJ",
                    "http://upload.nhacdj.vn/Nonstop/2016/3/4/128/NhacDJ.vn-Nontop-Vit-Mix-Vy-L-Ta-Chia-Tay-DJ-Ribo-Remix1457087753.mp3",
                    "http://upload.nhacdj.vn/Photo/2014/12/30/DJTitBanRon/hinhchunhat.jpg"));

            listSong.add(new Song(" Nonstop - Electro House - Cool", "Triệu Muzik",
                    "http://upload.nhacdj.vn/Nonstop/2016/2/10/128/NhacDJ.vn-Nonstop-Electro-House-Cool-DJ-Long-Eragon-Mix1455072991.mp3",
                    "http://upload.nhacdj.vn/Photo/2016/3/4/NgocTrinh/thumb.jpg"));

            listSong.add(new Song("Nonstop - Best Tracks Of The Year 2016", "DJ Young Nguyễn",
                    "http://upload.nhacdj.vn/Nonstop/2016/1/6/128/NhacDJ.vn-Nonstop-Best-Tracks-Of-The-Year-2016-Vol-1-DJ-Triu-Muzik-ft-DJ-Chut-Sc-Sn-ft-DJ-Trang-Siu-Nhn-Mix1452055453.mp3",
                    "http://nhacdj.vn/images/misc/avatar-dj-new.jpg"));

            listSong.add(new Song("Dang cap Chau Au", "Nhạc DJ",
                    "http://upload.nhacdj.vn//Nonstop/2016/3/19/128/NhacDJ.vn-Nonstop-X-Nhc-ng-Cp-Xung-o-Thi-Ri-DJ-Phc-Tu-97-Mix1458360571.mp3",
                    "http://upload.nhacdj.vn/Photo/2016/3/19/DoLot2/thumb.jpg"));

            listSong.add(new Song("Việt Mixx - Ta chia tay nhau đi", "Nhạc DJ",
                    "http://upload.nhacdj.vn/Nonstop/2016/3/4/128/NhacDJ.vn-Nontop-Vit-Mix-Vy-L-Ta-Chia-Tay-DJ-Ribo-Remix1457087753.mp3",
                    "http://upload.nhacdj.vn/Photo/2014/12/30/DJTitBanRon/hinhchunhat.jpg"));

            listSong.add(new Song("Nonstop - Phiêu Theo Điệu Nhạc 2016", "Triệu Muzik",
                    "http://upload.nhacdj.vn//Nonstop/2016/3/19/128/NhacDJ.vn-Nonstop-X-Nhc-ng-Cp-Xung-o-Thi-Ri-DJ-Phc-Tu-97-Mix1458360571.mp3",
                    "http://upload.nhacdj.vn/Photo/2016/3/4/NgocTrinh/thumb.jpg"));

            listSong.add(new Song("Như Ngày Hôm Qua", "DJ Young Nguyễn",
                    "http://upload.nhacdj.vn/Nonstop/2014/10/29/128/[NhacDJ.vn] - Nonstop-Vit-Mix-Hoa-Anh-Tc-DJ-HYT-Remix1414482835 [NhacDJ.vn].mp3",
                    "http://nhacdj.vn/images/misc/avatar-dj-new.jpg"));

            listSong.add(new Song("Dang cap Chau Au", "Nhạc DJ",
                    "http://upload.nhacdj.vn//Nonstop/2016/3/19/128/NhacDJ.vn-Nonstop-X-Nhc-ng-Cp-Xung-o-Thi-Ri-DJ-Phc-Tu-97-Mix1458360571.mp3",
                    "http://upload.nhacdj.vn/Photo/2016/3/19/DoLot2/thumb.jpg"));

            listSong.add(new Song("Nhac Trẻ không lời", "Nhạc DJ",
                    "http://upload.nhacdj.vn//Nonstop/2016/2/29/128/NhacDJ.vn-Nonstop-Vit-Mix-Tuyn-Tp-Cc-Ca-Khc-Nhc-Tr-2016-DJ-L-Lo-i-Mix1456729728.mp3",
                    "http://upload.nhacdj.vn/Photo/2016/3/22/CoDau/thumb.jpg"));
            listSong.add(new Song("Việt Mixx - Ta chia tay nhau đi", "Nhạc DJ",
                    "http://upload.nhacdj.vn/Nonstop/2016/3/4/128/NhacDJ.vn-Nontop-Vit-Mix-Vy-L-Ta-Chia-Tay-DJ-Ribo-Remix1457087753.mp3",
                    "http://upload.nhacdj.vn/Photo/2014/12/30/DJTitBanRon/hinhchunhat.jpg"));

            listSong.add(new Song("Nonstop - Phiêu Theo Điệu Nhạc 2016", "Triệu Muzik",
                    "http://upload.nhacdj.vn//Nonstop/2016/3/19/128/NhacDJ.vn-Nonstop-X-Nhc-ng-Cp-Xung-o-Thi-Ri-DJ-Phc-Tu-97-Mix1458360571.mp3",
                    "http://upload.nhacdj.vn/Photo/2016/3/4/NgocTrinh/thumb.jpg"));

            listSong.add(new Song("Nonstop - Việt Mix - Khi Bên Anh Em Thấy Điều Gì", "DJ Young Nguyễn",
                    "http://upload.nhacdj.vn/Nonstop/2015/10/10/128/NhacDJ.vn-Nonstop-Vit-Mix-Khi-Bn-Anh-Em-Thy-iu-G-DJ-Young-Mix1444471935.mp3",
                    "http://nhacdj.vn/images/misc/avatar-dj-new.jpg"));

            listSong.add(new Song("Nonstop - Vina House - Tuổi Gì Familly", "Nhạc DJ",
                    "http://upload.nhacdj.vn//Nonstop/2016/3/19/128/NhacDJ.vn-Nonstop-X-Nhc-ng-Cp-Xung-o-Thi-Ri-DJ-Phc-Tu-97-Mix1458360571.mp3",
                    "http://upload.nhacdj.vn/Photo/2016/3/19/DoLot2/thumb.jpg"));

            listSong.add(new Song("Nhac Trẻ không lời", "Nhạc DJ",
                    "http://upload.nhacdj.vn/Nonstop/2014/10/27/128/[NhacDJ.vn] - Nonstop-Vina-House-Tui-G-Familly-DJ-5STYLE-Remix1414399903 [NhacDJ.vn].mp3",
                    "http://upload.nhacdj.vn/Photo/2016/3/22/CoDau/thumb.jpg"));
            listSong.add(new Song("Việt Mixx - Ta chia tay nhau đi", "Nhạc DJ",
                    "http://upload.nhacdj.vn/Nonstop/2016/3/4/128/NhacDJ.vn-Nontop-Vit-Mix-Vy-L-Ta-Chia-Tay-DJ-Ribo-Remix1457087753.mp3",
                    "http://upload.nhacdj.vn/Photo/2014/12/30/DJTitBanRon/hinhchunhat.jpg"));

            listSong.add(new Song(" Nonstop - Electro House - Cool", "Triệu Muzik",
                    "http://upload.nhacdj.vn/Nonstop/2016/2/10/128/NhacDJ.vn-Nonstop-Electro-House-Cool-DJ-Long-Eragon-Mix1455072991.mp3",
                    "http://upload.nhacdj.vn/Photo/2016/3/4/NgocTrinh/thumb.jpg"));

            listSong.add(new Song("Nonstop - Best Tracks Of The Year 2016", "DJ Young Nguyễn",
                    "http://upload.nhacdj.vn/Nonstop/2016/1/6/128/NhacDJ.vn-Nonstop-Best-Tracks-Of-The-Year-2016-Vol-1-DJ-Triu-Muzik-ft-DJ-Chut-Sc-Sn-ft-DJ-Trang-Siu-Nhn-Mix1452055453.mp3",
                    "http://nhacdj.vn/images/misc/avatar-dj-new.jpg"));

            listSong.add(new Song("Dang cap Chau Au", "Nhạc DJ",
                    "http://upload.nhacdj.vn//Nonstop/2016/3/19/128/NhacDJ.vn-Nonstop-X-Nhc-ng-Cp-Xung-o-Thi-Ri-DJ-Phc-Tu-97-Mix1458360571.mp3",
                    "http://upload.nhacdj.vn/Photo/2016/3/19/DoLot2/thumb.jpg"));

            listSong.add(new Song("Việt Mixx - Ta chia tay nhau đi", "Nhạc DJ",
                    "http://upload.nhacdj.vn/Nonstop/2016/3/4/128/NhacDJ.vn-Nontop-Vit-Mix-Vy-L-Ta-Chia-Tay-DJ-Ribo-Remix1457087753.mp3",
                    "http://upload.nhacdj.vn/Photo/2014/12/30/DJTitBanRon/hinhchunhat.jpg"));

            listSong.add(new Song("Nonstop - Phiêu Theo Điệu Nhạc 2016", "Triệu Muzik",
                    "http://upload.nhacdj.vn//Nonstop/2016/3/19/128/NhacDJ.vn-Nonstop-X-Nhc-ng-Cp-Xung-o-Thi-Ri-DJ-Phc-Tu-97-Mix1458360571.mp3",
                    "http://upload.nhacdj.vn/Photo/2016/3/4/NgocTrinh/thumb.jpg"));

            listSong.add(new Song("Như Ngày Hôm Qua", "DJ Young Nguyễn",
                    "http://upload.nhacdj.vn/Nonstop/2014/10/29/128/[NhacDJ.vn] - Nonstop-Vit-Mix-Hoa-Anh-Tc-DJ-HYT-Remix1414482835 [NhacDJ.vn].mp3",
                    "http://nhacdj.vn/images/misc/avatar-dj-new.jpg"));

            listSong.add(new Song("Dang cap Chau Au", "Nhạc DJ",
                    "http://upload.nhacdj.vn//Nonstop/2016/3/19/128/NhacDJ.vn-Nonstop-X-Nhc-ng-Cp-Xung-o-Thi-Ri-DJ-Phc-Tu-97-Mix1458360571.mp3",
                    "http://upload.nhacdj.vn/Photo/2016/3/19/DoLot2/thumb.jpg"));

            listSong.add(new Song("Nhac Trẻ không lời", "Nhạc DJ",
                    "http://upload.nhacdj.vn//Nonstop/2016/2/29/128/NhacDJ.vn-Nonstop-Vit-Mix-Tuyn-Tp-Cc-Ca-Khc-Nhc-Tr-2016-DJ-L-Lo-i-Mix1456729728.mp3",
                    "http://upload.nhacdj.vn/Photo/2016/3/22/CoDau/thumb.jpg"));
            listSong.add(new Song("Việt Mixx - Ta chia tay nhau đi", "Nhạc DJ",
                    "http://upload.nhacdj.vn/Nonstop/2016/3/4/128/NhacDJ.vn-Nontop-Vit-Mix-Vy-L-Ta-Chia-Tay-DJ-Ribo-Remix1457087753.mp3",
                    "http://upload.nhacdj.vn/Photo/2014/12/30/DJTitBanRon/hinhchunhat.jpg"));

            listSong.add(new Song("Nonstop - Phiêu Theo Điệu Nhạc 2016", "Triệu Muzik",
                    "http://upload.nhacdj.vn//Nonstop/2016/3/19/128/NhacDJ.vn-Nonstop-X-Nhc-ng-Cp-Xung-o-Thi-Ri-DJ-Phc-Tu-97-Mix1458360571.mp3",
                    "http://upload.nhacdj.vn/Photo/2016/3/4/NgocTrinh/thumb.jpg"));

            listSong.add(new Song("Nonstop - Việt Mix - Khi Bên Anh Em Thấy Điều Gì", "DJ Young Nguyễn",
                    "http://upload.nhacdj.vn/Nonstop/2015/10/10/128/NhacDJ.vn-Nonstop-Vit-Mix-Khi-Bn-Anh-Em-Thy-iu-G-DJ-Young-Mix1444471935.mp3",
                    "http://nhacdj.vn/images/misc/avatar-dj-new.jpg"));

            listSong.add(new Song("Nonstop - Vina House - Tuổi Gì Familly", "Nhạc DJ",
                    "http://upload.nhacdj.vn//Nonstop/2016/3/19/128/NhacDJ.vn-Nonstop-X-Nhc-ng-Cp-Xung-o-Thi-Ri-DJ-Phc-Tu-97-Mix1458360571.mp3",
                    "http://upload.nhacdj.vn/Photo/2016/3/19/DoLot2/thumb.jpg"));

            listSong.add(new Song("Nhac Trẻ không lời", "Nhạc DJ",
                    "http://upload.nhacdj.vn/Nonstop/2014/10/27/128/[NhacDJ.vn] - Nonstop-Vina-House-Tui-G-Familly-DJ-5STYLE-Remix1414399903 [NhacDJ.vn].mp3",
                    "http://upload.nhacdj.vn/Photo/2016/3/22/CoDau/thumb.jpg"));
            listSong.add(new Song("Việt Mixx - Ta chia tay nhau đi", "Nhạc DJ",
                    "http://upload.nhacdj.vn/Nonstop/2016/3/4/128/NhacDJ.vn-Nontop-Vit-Mix-Vy-L-Ta-Chia-Tay-DJ-Ribo-Remix1457087753.mp3",
                    "http://upload.nhacdj.vn/Photo/2014/12/30/DJTitBanRon/hinhchunhat.jpg"));

            listSong.add(new Song(" Nonstop - Electro House - Cool", "Triệu Muzik",
                    "http://upload.nhacdj.vn/Nonstop/2016/2/10/128/NhacDJ.vn-Nonstop-Electro-House-Cool-DJ-Long-Eragon-Mix1455072991.mp3",
                    "http://upload.nhacdj.vn/Photo/2016/3/4/NgocTrinh/thumb.jpg"));

            listSong.add(new Song("Nonstop - Best Tracks Of The Year 2016", "DJ Young Nguyễn",
                    "http://upload.nhacdj.vn/Nonstop/2016/1/6/128/NhacDJ.vn-Nonstop-Best-Tracks-Of-The-Year-2016-Vol-1-DJ-Triu-Muzik-ft-DJ-Chut-Sc-Sn-ft-DJ-Trang-Siu-Nhn-Mix1452055453.mp3",
                    "http://nhacdj.vn/images/misc/avatar-dj-new.jpg"));

            listSong.add(new Song("Dang cap Chau Au", "Nhạc DJ",
                    "http://upload.nhacdj.vn//Nonstop/2016/3/19/128/NhacDJ.vn-Nonstop-X-Nhc-ng-Cp-Xung-o-Thi-Ri-DJ-Phc-Tu-97-Mix1458360571.mp3",
                    "http://upload.nhacdj.vn/Photo/2016/3/19/DoLot2/thumb.jpg"));
        }
        return listSong;
    }

    public static int getSelectedItem() {
        return selectedItem;
    }

    public static void setSelectedItem(int position) {
        selectedItem = position;
    }
}
