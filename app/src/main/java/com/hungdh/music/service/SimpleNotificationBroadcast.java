package com.hungdh.music.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class SimpleNotificationBroadcast extends BroadcastReceiver {
    public SimpleNotificationBroadcast() {
    }

    public static final String NOTIFY_PREVIOUS = "com.tutorialsface.notificationdemo.previous";
    public static final String NOTIFY_DELETE = "com.tutorialsface.notificationdemo.delete";
    public static final String NOTIFY_PAUSE = "com.tutorialsface.notificationdemo.pause";
    public static final String NOTIFY_PLAY = "com.tutorialsface.notificationdemo.play";
    public static final String NOTIFY_NEXT = "com.tutorialsface.notificationdemo.next";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(NOTIFY_PLAY)) {
            Log.v("SimpleNoti", "NOTIFY_PLAY");
        } else if (intent.getAction().equals(NOTIFY_PAUSE)) {
            Log.v("SimpleNoti", "NOTIFY_PAUSE");
        } else if (intent.getAction().equals(NOTIFY_NEXT)) {
            Log.v("SimpleNoti", "NOTIFY_NEXT");
        } else if (intent.getAction().equals(NOTIFY_DELETE)) {
            Log.v("SimpleNoti", "NOTIFY_DELETE");
        }else if (intent.getAction().equals(NOTIFY_PREVIOUS)) {
            Log.v("SimpleNoti", "NOTIFY_PREVIOUS");
        }
    }
}
