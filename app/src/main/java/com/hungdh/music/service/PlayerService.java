package com.hungdh.music.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.hungdh.music.R;
import com.hungdh.music.model.Song;

import java.io.File;
import java.io.IOException;

import static com.hungdh.music.activity.PlayerActivity.CURRENT_POSITION;
import static com.hungdh.music.activity.PlayerActivity.DURATION;
import static com.hungdh.music.activity.PlayerActivity.PROGRESS;
import static com.hungdh.music.activity.PlayerActivity.SEEK_BAR_POSITION;
import static com.hungdh.music.activity.PlayerActivity.SEND_DURATION;
import static com.hungdh.music.activity.PlayerActivity.SEND_LOAD_COMPLETE;
import static com.hungdh.music.activity.PlayerActivity.SEND_NEXT;
import static com.hungdh.music.activity.PlayerActivity.SEND_PLAY_COMPLETE;
import static com.hungdh.music.activity.PlayerActivity.SEND_PREV;
import static com.hungdh.music.activity.PlayerActivity.SEND_PROGRESS;
import static com.hungdh.music.activity.PlayerActivity.SEND_SEEK_BAR;
import static com.hungdh.music.activity.PlayerActivity.SEND_SONG;
import static com.hungdh.music.activity.PlayerActivity.SEND_STATUS;

public class PlayerService extends Service {

    int NOTIFICATION_ID = 1111;
    public static final String SERVICE_NAME = "com.hungdh.music.service.PlayerService";
    public static final String NOTIFY_PREVIOUS = "com.hungdh.music.previous";
    public static final String NOTIFY_DELETE = "com.hungdh.music.delete";
    public static final String NOTIFY_PAUSE = "com.hungdh.music.pause";
    public static final String NOTIFY_PLAY = "com.hungdh.music.play";
    public static final String NOTIFY_NEXT = "com.hungdh.music.next";

    private Song mSong;

    private Thread thread, threadUpdateTimer;
    private Handler handler;
    private MediaPlayer mediaPlayer;

    private UpdateTimerRunable updateTimerRunable;
    private BroadcastReceiver notificationReceiver;

    private NotificationManager notificationManager;
    private final String TAG = getClass().getSimpleName();

    private MediaPlayer.OnPreparedListener onPreparedListener = new MediaPlayer.OnPreparedListener() {
        @Override
        public void onPrepared(MediaPlayer mp) {
            sendDurationToActivity(mp.getDuration());
            sendLoadComplete();
            updateTimer();
        }
    };

    private MediaPlayer.OnCompletionListener onCompletionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            sendPlayComplete();
        }
    };

    private MediaPlayer.OnSeekCompleteListener onSeekCompleteListener = new MediaPlayer.OnSeekCompleteListener() {
        @Override
        public void onSeekComplete(MediaPlayer mp) {
            sendLoadComplete();
        }
    };

    private MediaPlayer.OnErrorListener onErrorListener = new MediaPlayer.OnErrorListener() {
        @Override
        public boolean onError(MediaPlayer mp, int what, int extra) {
            String msg = "";
            if (extra == MediaPlayer.MEDIA_ERROR_TIMED_OUT) {
                msg = "Time out error";
            } else if (what == MediaPlayer.MEDIA_ERROR_SERVER_DIED) {
                msg = "Server die";
            } else {
                msg = "Unknow error";
            }
            sendPlayComplete();
            Log.d(TAG, msg);
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
            mp.stop();
            mp.release();
            return false;
        }
    };

    public PlayerService() {
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mSong = (Song) intent.getBundleExtra(SEND_SONG).getSerializable(SEND_SONG);

        registerIntentFilter();

        playMedia();
        showNotification();

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver());

        if (updateTimerRunable != null) {
            updateTimerRunable.onPause();
        }
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
            }
        }
    }

    private void showNotification() {
        RemoteViews customView = new RemoteViews(getPackageName(), R.layout.custom_notification);

        Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_music)
                .setContentTitle("Custom Big View").build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.contentView = customView;
        notification.contentView.setTextViewText(R.id.textSongName, mSong.getSongTitle());
        notification.contentView.setTextViewText(R.id.textAlbumName, mSong.getSongAuthor());

        setListeners(customView);

        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notification.flags |= Notification.FLAG_ONGOING_EVENT;
        startForeground(1, notification);

    }

    private void setListeners(RemoteViews remoteViews){
        Intent previous = new Intent(NOTIFY_PREVIOUS);
        Intent delete = new Intent(NOTIFY_DELETE);
        Intent pause = new Intent(NOTIFY_PAUSE);
        Intent next = new Intent(NOTIFY_NEXT);
        Intent play = new Intent(NOTIFY_PLAY);

        PendingIntent pPrevious = PendingIntent.getBroadcast(this, 0, previous, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.btnPrevious, pPrevious);

        PendingIntent pDelete = PendingIntent.getBroadcast(this, 0, delete, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.btnDelete, pDelete);

        PendingIntent pPause = PendingIntent.getBroadcast(this, 0, pause, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.btnPause, pPause);

        PendingIntent pNext = PendingIntent.getBroadcast(this, 0, next, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.btnNext, pNext);

        PendingIntent pPlay = PendingIntent.getBroadcast(this, 0, play, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.btnPlay, pPlay);
    }



    private void registerIntentFilter() {
        IntentFilter filter = new IntentFilter(SEND_STATUS);
        filter.addAction(SEND_SEEK_BAR);
        filter.addAction(SEND_NEXT);
        filter.addAction(SEND_PREV);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver(), filter);
    }

    private BroadcastReceiver receiver() {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {
                    case SEND_STATUS:
                        if (mediaPlayer.isPlaying()) {
                            mediaPlayer.pause();
                            updateTimerRunable.onPause();
                        } else {
                            mediaPlayer.start();
                            updateTimerRunable.onResume();
                        }
                        break;
                    case SEND_SEEK_BAR:
                        int position = intent.getIntExtra(SEEK_BAR_POSITION, 0);
                        mediaPlayer.seekTo(position * mediaPlayer.getDuration() / 100);
                        break;
//                    case SEND_
                }
            }
        };
    }


    private void playMedia() {
        if (handler == null) {
            thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Looper.prepare();
                    handler = new Handler();
                    File f = new File(mSong.getSongUrl());
//                    if (f.exists() && f.isFile())
//                        handler.post(playAudioFromFile());
//                    else
                    handler.post(playAudioFromURL());
                    Looper.loop();
                }
            });
            thread.start();
        } else {
            File f = new File(mSong.getSongUrl());
//            if (f.exists() && f.isFile())
//                handler.post(playAudioFromFile());
//            else
            handler.post(playAudioFromURL());
        }
    }

    private Runnable playAudioFromURL() {
        return new Runnable() {
            @Override
            public void run() {
                if (mediaPlayer != null) {
                    mediaPlayer.release();
                }
                mediaPlayer = new MediaPlayer();
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                try {
                    mediaPlayer.setDataSource(mSong.getSongUrl());
                    mediaPlayer.setOnPreparedListener(onPreparedListener);
                    mediaPlayer.setOnCompletionListener(onCompletionListener);
                    mediaPlayer.setOnSeekCompleteListener(onSeekCompleteListener);
                    mediaPlayer.setOnErrorListener(onErrorListener);
                    mediaPlayer.setLooping(true);
                    mediaPlayer.prepare();
                    mediaPlayer.start();

                } catch (IOException e) {
                    Log.w(TAG, e.toString());
                    e.printStackTrace();
                } catch (IllegalArgumentException ex) {
                    Log.w(TAG, "MediaPlayer IllegalArgumentException: " + ex);
                } catch (IllegalStateException ex) {
                    Log.w(TAG, "MediaPlayer IllegalStateException: " + ex);
                }
            }
        };
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * SEND total time, current time, progress
     */

    private void sendDurationToActivity(int duration) {
        Intent intent = new Intent(SEND_DURATION);
        intent.putExtra(DURATION, duration);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void sendProgressToActivity(int progress, int current_possition) {
        Intent intent = new Intent(SEND_PROGRESS);
        intent.putExtra(PROGRESS, progress);
        intent.putExtra(CURRENT_POSITION, current_possition);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void sendLoadComplete() {
        Intent intent = new Intent(SEND_LOAD_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void sendPlayComplete() {
        Intent intent = new Intent(SEND_PLAY_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void updateTimer() {
        updateTimerRunable = new UpdateTimerRunable();
        threadUpdateTimer = new Thread(updateTimerRunable);
        threadUpdateTimer.start();
    }

    class UpdateTimerRunable implements Runnable {
        private Object pauseLock;
        private boolean isPause;
        private volatile boolean isStop = false;

        public UpdateTimerRunable() {
            isPause = false;
            pauseLock = new Object();
        }

        @Override
        public void run() {
            while (mediaPlayer.isPlaying() && !isStop) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                sendProgressToActivity(mediaPlayer.getCurrentPosition() * 100 / mediaPlayer.getDuration(),
                        mediaPlayer.getCurrentPosition());
                // Hand continue play after click pause
                synchronized (pauseLock) {
                    while (isPause)
                        try {
                            pauseLock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                }
            }
        }

        public void onPause() {
            synchronized (pauseLock) {
                isPause = true;
            }
        }

        public void onResume() {
            synchronized (pauseLock) {
                isPause = false;
                pauseLock.notifyAll();
            }
        }

        public void onStop() {
            isStop = true;
        }
    }

}
