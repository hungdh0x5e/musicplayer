package com.hungdh.music.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by hungdh on 01/04/2016.
 */
public class SharedManager {
    private static SharedManager mInstance;
    private static SharedPreferences shared;

    public static synchronized SharedManager getInstance(Context context) {
        if (mInstance == null)
            mInstance = new SharedManager();
        if (shared == null)
            shared = PreferenceManager.getDefaultSharedPreferences(context);
        return mInstance;
    }

    private SharedManager() {

    }

    public static String getString(String keyword) {
        return shared.getString(keyword, "");
    }

    public static void putString(String keyword, String value) {
        shared.edit().putString(keyword, value).apply();
    }

    public static int getInt(String keyword) {
        return shared.getInt(keyword, 0);
    }

    public static void putInt(String keyword, int value) {
        shared.edit().putInt(keyword, value).apply();
    }

    public static boolean getBoolean(String keyword) {
        return shared.getBoolean(keyword, false);
    }

    public static void putBoolean(String keyword, boolean value) {
        shared.edit().putBoolean(keyword, value).apply();
    }
}
