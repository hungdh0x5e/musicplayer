package com.hungdh.music.model;

import java.io.Serializable;

/**
 * Created by hungdh on 25/03/2016.
 */
public class Song implements Serializable{
    private String songTitle;
    private String songAuthor;
    private String songUrl;
    private String songImage;

    public Song(String songTitle, String songAuthor, String songUrl, String songImage) {
        this.songTitle = songTitle;
        this.songAuthor = songAuthor;
        this.songUrl = songUrl;
        this.songImage = songImage;
    }

    public String getSongTitle() {
        return songTitle;
    }

    public void setSongTitle(String songTitle) {
        this.songTitle = songTitle;
    }

    public String getSongAuthor() {
        return songAuthor;
    }

    public void setSongAuthor(String songAuthor) {
        this.songAuthor = songAuthor;
    }

    public String getSongUrl() {
        return songUrl;
    }

    public void setSongUrl(String songUrl) {
        this.songUrl = songUrl;
    }

    public String getSongImage() {
        return songImage;
    }

    public void setSongImage(String songImage) {
        this.songImage = songImage;
    }
}
